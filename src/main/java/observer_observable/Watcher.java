package observer_observable;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by Marek on 18.09.2017.
 */
public class Watcher implements Observer {
    private int id;
    private int threshold;

    public Watcher(int id,int threshold) {
        this.id =id;
        this.threshold = threshold;
    }

    @Override
    public void update(Observable o, Object arg) {
            if (arg instanceof News){
                News news=(News) arg;

                System.out.println("Watcher "+id+": "+news.getMessage());
                if (news.getThreshold()>threshold){
                    System.out.println("Watcher "+id+" started panic!");
                }
            }
    }
}
