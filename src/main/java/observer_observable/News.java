package observer_observable;

/**
 * Created by Marek on 18.09.2017.
 */
public class News {
    private int threshold;
    private String message;

    public int getThreshold() {
        return threshold;
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public News(int threshold, String message) {
        this.threshold = threshold;
        this.message=message;

    }
}
