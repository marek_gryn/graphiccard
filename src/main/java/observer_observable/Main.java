package observer_observable;

import java.util.Scanner;

/**
 * Created by Marek on 18.09.2017.
 */
public class Main {
    public static void main(String[] args) {


        String command="";
        String[] splited;
        Scanner scanner=new Scanner(System.in);


        NewsStation newsStation=new NewsStation();
        Watcher watcher1=new Watcher(1,5);
        Watcher watcher2=new Watcher(2,2);
        Watcher watcher3=new Watcher(3,4);
        Watcher watcher4=new Watcher(4,8);

        newsStation.addObserver(watcher1);
        newsStation.addObserver(watcher2);
        newsStation.addObserver(watcher3);
        newsStation.addObserver(watcher4);



        while(true){
            System.out.println("insert command");
            command=scanner.nextLine();
            if(command.equals("quit")){
                break;
            }
            splited=command.split(" ",2);
            newsStation.broadcastNews(new News(Integer.parseInt(splited[0]), splited[1]));

//            if(splited[0].equals("set")){
//                if(splited[1].equals("hd")){
//
//                }else if (splited[1].equals("mid")){
//                }else if (splited[1].equals("low")){
//
//                }
//            }
//
//            if(command.equals("show frame")){
//
//            }

        }
    }
}
