package observer_observable;



/**
 * Created by Marek on 18.09.2017.
 */
public class NewsStation extends java.util.Observable{

    public void broadcastNews (News news){
        setChanged();
        notifyObservers(news);
    }

}
