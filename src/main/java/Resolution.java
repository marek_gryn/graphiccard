/**
 * Created by Marek on 18.09.2017.
 */
public enum Resolution {
    HD,
    FHD,
    ULTRA_HD;
}
