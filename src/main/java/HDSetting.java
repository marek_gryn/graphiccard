/**
 * Created by Marek on 18.09.2017.
 */
public class HDSetting implements GraphicsSetting {
    private int frame[][]=new int[20][32];
    private Resolution resolution=Resolution.HD;
    private Gamma gamma=Gamma.COLORS_256;
    private AspectRatio aspectRatio=AspectRatio.RATIO_16_9;

    public int[][] getFrame() {
        return frame;
    }

    public void setFrame(int[][] frame) {
        this.frame = frame;
    }

    public Resolution getResolution() {
        return resolution;
    }

    public void setResolution(Resolution resolution) {
        this.resolution = resolution;
    }

    public Gamma getGamma() {
        return gamma;
    }

    public void setGamma(Gamma gamma) {
        this.gamma = gamma;
    }

    public AspectRatio getAspectRatio() {
        return aspectRatio;
    }

    public void setAspectRatio(AspectRatio aspectRatio) {
        this.aspectRatio = aspectRatio;
    }


    @Override
    public void getNeededProcessingPower() {

    }

    @Override
    public void processFrame() {
        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < 32; j++) {
                frame[i][j]=3;
            }
        }

        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < 32; j++) {
                System.out.print(frame[i][j]);
            }
            System.out.println();
        }
    }
}
