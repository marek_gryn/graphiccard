/**
 * Created by Marek on 18.09.2017.
 */
public enum Gamma {
    BIT_32,
    BIT_16,
    MONO,
    COLORS_256;

}
