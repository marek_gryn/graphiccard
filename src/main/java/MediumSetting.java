/**
 * Created by Marek on 18.09.2017.
 */
public class MediumSetting implements GraphicsSetting{
    private int frame[][]=new int[15][24];
    private Resolution resolution=Resolution.FHD;
    private Gamma gamma=Gamma.BIT_16;
    private AspectRatio aspectRatio=AspectRatio.RATIO_16_9;

    public int[][] getFrame() {
        return frame;
    }

    public void setFrame(int[][] frame) {
        this.frame = frame;
    }

    public Resolution getResolution() {
        return resolution;
    }

    public void setResolution(Resolution resolution) {
        this.resolution = resolution;
    }

    public Gamma getGamma() {
        return gamma;
    }

    public void setGamma(Gamma gamma) {
        this.gamma = gamma;
    }

    public AspectRatio getAspectRatio() {
        return aspectRatio;
    }

    public void setAspectRatio(AspectRatio aspectRatio) {
        this.aspectRatio = aspectRatio;
    }


    @Override
    public void getNeededProcessingPower() {

    }

    @Override
    public void processFrame() {
        for (int i = 0; i < 15; i++) {
            for (int j = 0; j < 24; j++) {
                frame[i][j]=2;
            }
        }

        for (int i = 0; i < 15; i++) {
            for (int j = 0; j < 24; j++) {
                System.out.print(frame[i][j]);
            }
            System.out.println();
        }
    }
}
