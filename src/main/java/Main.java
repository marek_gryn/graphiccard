import java.util.Scanner;

/**
 * Created by Marek on 18.09.2017.
 */
public class Main {
    public static void main(String[] args) {
        GraphicsCard graphicsCard=new GraphicsCard("addd");
        String command="";
        String[] splited;
        Scanner scanner=new Scanner(System.in);

        while(true){
            System.out.println("insert command");
            command=scanner.nextLine();
            if(command.equals("quit")){
                break;
            }
            splited=command.split(" ");

            if(splited[0].equals("set")){
                if(splited[1].equals("hd")){
                    graphicsCard.setGraphicsSetting("hd");
                }else if (splited[1].equals("mid")){
                    graphicsCard.setGraphicsSetting("mid");
                }else if (splited[1].equals("low")){
                    graphicsCard.setGraphicsSetting("low");
                }
            }

            if(command.equals("show frame")){
                graphicsCard.processFrame();
            }

        }

    }
}
