/**
 * Created by Marek on 18.09.2017.
 */
public enum AspectRatio {
    RATIO_16_10,
    RATIO_16_9,
    RATIO_4_3,
    RATIO_3_2;
}
