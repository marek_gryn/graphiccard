/**
 * Created by Marek on 18.09.2017.
 */
public class GraphicsCard {
    private String name;
    private GraphicsSetting graphicsSetting;

    public GraphicsCard(String name) {
        this.name = name;
        graphicsSetting=new LowSetting();
    }

    public void setGraphicsSetting (String tekst){
        if(tekst.equals("hd")){
            graphicsSetting=new HDSetting();
        }else if(tekst.equals("mid")){
            graphicsSetting=new MediumSetting();
        }else if(tekst.equals("low")) {
            graphicsSetting = new LowSetting();
        }else {
            System.out.println("incorrect command");
        }
    }

    public void processFrame(){
        graphicsSetting.processFrame();
    }
}
