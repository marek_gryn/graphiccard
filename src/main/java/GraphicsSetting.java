/**
 * Created by Marek on 18.09.2017.
 */
public interface GraphicsSetting {
    void getNeededProcessingPower();
    void processFrame();
}
